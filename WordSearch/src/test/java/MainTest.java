
import org.example.Main;
import org.junit.Test;


import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;


public class MainTest {
    char[][] wordSearch = {
            {'A', 'B', 'C', 'D'},
            {'E', 'F', 'G', 'H'},
            {'I', 'J', 'K', 'L'}
    };
    @Test
    public void testHorizontalSearching() throws FileNotFoundException {

        //Following lines were added to obtain at least 80% Code coverage
        String files = "src/main/resources/sample-input.txt";
        Main.wordSearching(files);


        String horizontalResult1 = Main.searchWordSearch("ABC", wordSearch);
        assertEquals("ABC 0:0  0:2", horizontalResult1);
        String horizontalResult2 = Main.searchWordSearch("GFE", wordSearch);
        assertEquals("GFE 1:2  1:0",horizontalResult2);

    }

    @Test
    public void testVerticalSearching() throws FileNotFoundException {

        String verticalResutl1 = Main.searchWordSearch("AEI", wordSearch);
        assertEquals("AEI 0:0  2:0", verticalResutl1);
        String verticalResutl2 = Main.searchWordSearch("LHD", wordSearch);
        assertEquals("LHD 2:3  0:3", verticalResutl2);
    }
    @Test
    public void testDiagonalSearching() throws FileNotFoundException {
        String diagResutl1 = Main.searchWordSearch("AFK", wordSearch);
        assertEquals("AFK 0:0  2:2", diagResutl1);
        String diagResutl2 = Main.searchWordSearch("LGB", wordSearch);
        assertEquals("LGB 2:3  0:1", diagResutl2);
        String diagResutl3 = Main.searchWordSearch("IFC", wordSearch);
        assertEquals("IFC 2:0  0:2", diagResutl3);
        String diagResutl4 = Main.searchWordSearch("DGJ", wordSearch);
        assertEquals("DGJ 0:3  2:1", diagResutl4);
    }

}

