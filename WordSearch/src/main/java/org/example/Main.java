package org.example;
/*
*      The following code is my, Niya Elliott-Wade, solution to the alphabet-soup challenge
*      When the following code runs, it takes input from an input file located in the resources folder
*      and scans through the text to obtain a word search.  The input file is formatted with the dimensions
*      of the word search, the actual character structure that forms the actual word search and a list of words
*      to look for in the word search.  The code finds the word and returns the position of both its first letter
*      and its last letter in the following format : WORD 0:0 0:3
*
*      In this package I have also included unit test to test the functions and obtain at least 80% code coverage
* */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String files = "WordSearch/src/main/resources/sample-input.txt";
        wordSearching(files);
    }
    public static void wordSearching(String fileName) throws FileNotFoundException {
       /* This code starts by reading an input file from the resources
       folder */

        Scanner scanner = new Scanner(new FileReader(fileName));
        String inputFile = scanner.nextLine();

        /*Creates the actual word search from input file*/
        String[] grid = inputFile.split("x");
        int rows = Integer.parseInt(grid[0].trim());
        int cols = Integer.parseInt(grid[1].trim());

        /*Goes through the input file and assigns row and columns in
        a 2D array to create the word search*/
        char[][] wordSearch = new char[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                wordSearch[i][j] = scanner.next().charAt(0);
            }
        }
        /*Reads the remainder of the input file to get the words
        to find in the word search*/
        ArrayList<String> words = new ArrayList<>();
        while (scanner.hasNext()) {
            String wordToFind = scanner.next();

            words.add(wordToFind);
        }
        /* Iterates through the list of words to find and then returns output for words listed*/
        for (String word : words) {
            System.out.println(searchWordSearch(word, wordSearch));
        }

    }

    public static String searchWordSearch(String word, char[][] wordSearch){
        String result ="";
        //Checks for words in word search horizontally
        for (int i = 0; i < wordSearch.length; i++) {
            for (int j = 0; j <= wordSearch[i].length - word.length(); j++) {
                String rowString = new String(wordSearch[i], j, word.length());
                if (rowString.equals(word) ){
                    result = (i + ":" + j + "  " + i + ":" + (j + word.length() - 1));
                    break; // Break the loop after finding the word in the current row
                }
                //checks to see if word is reversed(backwards) in word search
                if(rowString.contentEquals(new StringBuilder(word).reverse())){
                    result = (i + ":" + (j + word.length() - 1) + "  "+i + ":" + j );
                    break;
                }
            }
        }
        //Checks for words in word search vertically
        for (int j = 0; j < wordSearch[0].length; j++) {
            for (int i = 0; i <= wordSearch.length - word.length(); i++) {
                StringBuilder columnString = new StringBuilder();
                for (int k = 0; k < word.length(); k++) {
                    columnString.append(wordSearch[i + k][j]);
                }
                if (columnString.toString().equals(word)){
                    result=(i + ":" + j + "  " + (i + word.length() - 1) + ":" + j);
                    break; // Break the loop after finding the word in the current column
                }
                if (columnString.reverse().toString().equals(word)) {
                    result=((i + word.length() - 1) + ":" + j + "  "+i + ":" + j );
                    break; // Break the loop after finding the word in the current column
                }
            }
        }
        // Search diagonally for the word (forward diagonal) -- left to right downward
        for (int i = 0; i <= wordSearch.length - word.length(); i++) {
            for (int j = 0; j <= wordSearch[i].length - word.length(); j++) {
                StringBuilder diagonalString = new StringBuilder();
                for (int k = 0; k < word.length(); k++) {
                    diagonalString.append(wordSearch[i + k][j + k]);
                }
                if (diagonalString.toString().equals(word)) {
                   result =( i + ":" + j + "  " + (i + word.length() - 1) + ":" + (j + word.length() - 1));
                    break;
                }
                if (diagonalString.reverse().toString().equals(word)) {
                    result= ((i + word.length() - 1) + ":" + (j + word.length() - 1) + "  "+ i + ":" + j);
                    break;
                }
            }
        }
        // Search diagonally for the word (reverse diagonal) left to right upward
        for (int i = 0; i <= wordSearch.length - word.length(); i++) {
            for (int j = word.length() - 1; j < wordSearch[i].length; j++) {
                StringBuilder diagonalString = new StringBuilder();
                for (int k = 0; k < word.length(); k++) {
                    diagonalString.append(wordSearch[i + k][j - k]);
                }
                if (diagonalString.toString().equals(word)) {
                   result = (i + ":" + j + "  " + (i + word.length() - 1) + ":" + (j - word.length() + 1));
                    break;
                }
                if (diagonalString.reverse().toString().equals(word)) {
                 result = ((i + word.length() - 1) + ":" + (j - word.length() + 1) + "  " + i + ":" + j);
                    break;
                }
            }
        }
        return word+" "+result;
    }
}
